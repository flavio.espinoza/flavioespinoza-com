import React from 'react'
// nodejs library that concatenates classes
import classNames from 'classnames'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
// core components
import Header from 'components/Header/Header.js'
import HeaderLinks from 'components/Header/HeaderLinks.js'
import Parallax from 'components/Parallax/Parallax.js'
import Footer from 'components/Footer/Footer.js'
import GridContainer from 'components/Grid/GridContainer.js'
import GridItem from 'components/Grid/GridItem.js'
import Button from 'components/CustomButtons/Button.js'
// sections for this page
import SectionDescription from 'views/PresentationPage/Sections/SectionDescription.js'
import SectionComponents from 'views/PresentationPage/Sections/SectionComponents.js'
import SectionCards from 'views/PresentationPage/Sections/SectionCards.js'
import SectionContent from 'views/PresentationPage/Sections/SectionContent.js'
import SectionSections from 'views/PresentationPage/Sections/SectionSections.js'
import SectionExamples from 'views/PresentationPage/Sections/SectionExamples.js'
import SectionFreeDemo from 'views/PresentationPage/Sections/SectionFreeDemo.js'
import SectionOverview from 'views/PresentationPage/Sections/SectionOverview.js'
import SectionPricing from 'views/PresentationPage/Sections/SectionPricing.js'

import presentationStyle from 'assets/jss/material-kit-pro-react/views/presentationStyle.js'

const useStyles = makeStyles(presentationStyle)

export default function PresentationPage() {
  const [subtitle] = React.useState('')
  React.useEffect(() => {
    window.scrollTo(0, 0)
    document.body.scrollTop = 0
  })
  const classes = useStyles()
  return (
    <div>
      <Header
        brand='Flavio Espinoza'
        links={<HeaderLinks dropdownHoverColor='info' />}
        fixed
        color='transparent'
        changeColorOnScroll={{
          height: 400,
          color: 'info',
        }}
      />
      <Parallax
        image={require('assets/img/bg-crucible-side-view.jpeg')}
        className={classes.parallax}
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={6}>
              <div className={classes.brand}>
                <h1>I build epic sh*t!</h1>
                <h3 className={classes.title}>{subtitle}</h3>
              </div>
            </GridItem> 
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <SectionDescription />
        <SectionComponents />
        <SectionCards />
        <SectionContent />
        <SectionSections />
        <SectionExamples />
        <SectionFreeDemo />
        <SectionOverview />
      </div>
      <SectionPricing />
      <Footer
        theme='white'
        content={
          <div>
            <div className={classes.left}>
              <a
                href='https://flavioespinoza.com'
                target='_blank'
                className={classes.footerBrand}
              >
                Flavio Espinoza
              </a>
            </div>
            <div className={classes.rightLinks}>
              <ul>
                <li>
                  <Button
                    href='https://twitter.com/CreativeTim?ref=creativetim'
                    target='_blank'
                    color='twitter'
                    justIcon
                    simple
                  >
                    <i className='fab fa-twitter' />
                  </Button>
                </li>
                <li>
                  <Button
                    href='https://dribbble.com/creativetim?ref=creativetim'
                    target='_blank'
                    color='dribbble'
                    justIcon
                    simple
                  >
                    <i className='fab fa-dribbble' />
                  </Button>
                </li>
                <li>
                  <Button
                    href='https://instagram.com/CreativeTimOfficial?ref=creativetim'
                    target='_blank'
                    color='instagram'
                    justIcon
                    simple
                  >
                    <i className='fab fa-instagram' />
                  </Button>
                </li>
              </ul>
            </div>
          </div>
        }
      />
    </div>
  )
}
