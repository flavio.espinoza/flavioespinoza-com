export default function (firstName, lastName) {
  if (firstName.toLowerCase() === `kevin`) {
    if (lastName.toLowerCase() ===  `burke`) {
      return {
        author: "Kevin Burke",
        quote: `"Flavio is a rare combination of talent and commitment with an intuitive understanding of 
                graphic visual communication and a propensity for creative problem-solving. Flavio considers the broad 
                ramifications of a concept even while he is designing the smallest detail."`,
        title: 'Principle, Parabola Architecture',
        website: 'https://parabola-architecture.com/',
        email: 'burke614@gmail.com'
      }
    }
  }
}